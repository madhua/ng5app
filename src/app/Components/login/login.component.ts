import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// import { LoginService } from '../Services/login/login.service';
//import { LoginService } from 'D:/gitprojects/ANgular5App/Ng5App/src/app/Services/login/login.service'
import { error } from 'selenium-webdriver';
import { LoginService } from '../../../../src/app/Services/login/login.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model : User = {username: '', password: ''};
  loading = false;
  error = '';

  constructor(private router : Router,
              private loginservice: LoginService) { }

  ngOnInit() {
  }

  login(){
    this.loading = true;
    this.loginservice.login(this.model.username, this.model.password).subscribe(result=>{
      if(result != null){
        //this.router.navigate(['home']);
      }
      else{
        this.error = 'username or password incorrect';
        this.loading = false;
      }
    },error=>{
      this.error = error;
      this.loading = false;
    })
  }

}

interface User {
  username: string,
  password: string
}
